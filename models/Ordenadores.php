<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordenadores".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property string|null $procesador
 * @property string|null $memoria
 * @property string|null $disco_duro
 * @property int|null $ethernet
 * @property int|null $wifi
 * @property string|null $video
 */
class Ordenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ethernet', 'wifi'], 'integer'],
            [['descripcion'], 'string', 'max' => 800],
            [['procesador', 'memoria', 'disco_duro', 'video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'procesador' => 'Procesador',
            'memoria' => 'Memoria Ram',
            'disco_duro' => 'Disco Duro',
            'ethernet' => 'Ethernet',
            'wifi' => 'Wifi',
            'video' => 'Tarjeta de Video',
        ];
    }
}
