﻿DROP DATABASE IF EXISTS practica8;
CREATE DATABASE practica8;
USE practica8;

CREATE TABLE ordenadores(
  id int AUTO_INCREMENT,
  descripcion varchar (800),
  procesador varchar (255),
  memoria varchar (255),
  disco_duro varchar (255),
  ethernet boolean,
  wifi boolean,
  video varchar (255),
  PRIMARY KEY (id)
);