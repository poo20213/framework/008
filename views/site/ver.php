<h1>Ver datos del ordenador</h1>
<?php

$this->params['breadcrumbs'][] = ['label' => 'Administrar', 'url' => ['administrar']];

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
        'model' => $model,
        'attributes' => [
        'id',
        'descripcion',
        'procesador',
        'memoria',
        'disco_duro',
        [
            'attribute' => 'ethernet',
            'format' => 'raw',
            'value' => function ($model){
                // colocar un checkbox deshabilitado
                //return Html::activeCheckbox($model, 'wifi', ['disabled'=>true]);
                    if($model->ethernet){
                        return '<i class="far fa-smile-beam"></i>';
                    }else{
                        return '<i class="far fa-dizzy"></i>';
                    }
            }
        ],
        [
            'attribute' => 'wifi',
            'format' => 'raw',
            'value' => function ($model){
                // colocar un checkbox deshabilitado
                //return Html::activeCheckbox($model, 'wifi', ['disabled'=>true]);
                    if($model->wifi){
                        return '<i class="fas fa-check"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
            }
        ],
        'video',        
    ]
]);


echo Html::a("Ver por bloques",
        ['site/verbloques','id'=>$model->id],
        ['class'=>"btn btn-primary"])
?>