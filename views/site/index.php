<?php

/* @var $this yii\web\View */

$this->title = 'Gestion ordenadores';
use yii\helpers\Html;
?>
<div class="site-index">
    
    <div class="body-content row">
        <div class="col-lg-4">
            <?=Html::img('@web/imgs/portada.jpg',[
                'class' => 'img-fluid circle',
                //'style' => 'border-radius:40px',
                'style' => 'border-radius:40px -webkit-box-shadow: 2px 2px 40px 10px #FF3F0F;box-shadow: 2px 2px 40px 10px #FF3F0F;'
                . '-webkit-border-radius: 50px 0;border-radius: 50px 0;'
            ])?>
            </div>
            
            <div class="col-lg-7 text-justify">

                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur.                                    
            </div>
        </div>
    </div>
</div>