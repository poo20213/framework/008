<?php
use yii\grid\GridView;
use yii\helpers\Html;


?>    
<div class="row">
    
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'descripcion',
        'procesador',
        'memoria',
        'disco_duro',
        [
            'attribute' => 'ethernet',
            'format' => 'raw',
            'value' => function ($model){
                // colocar un checkbox deshabilitado
                //return Html::activeCheckbox($model, 'wifi', ['disabled'=>true]);
                    if($model->ethernet){
                        return '<i class="far fa-smile-beam"></i>';
                    }else{
                        return '<i class="far fa-dizzy"></i>';
                    }
            }
        ],
        [
            'attribute' => 'wifi',
            'format' => 'raw',
            'value' => function ($model){
                // colocar un checkbox deshabilitado
                //return Html::activeCheckbox($model, 'wifi', ['disabled'=>true]);
                    if($model->wifi){
                        return '<i class="fas fa-check"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
            }
        ],
        'video',
            [
                'header' => 'Acciones',
                'headerOptions' => ["class" => "btn-link font-weight-bold"],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{insertar} {eliminar} {actualizar} {ver}',

                    'buttons'=>[    
                        'insertar' => function ($url,$model,$key) {
                                return Html::a('<i class="fas fa-folder-plus"></i>', 
                                        [
                                        "site/insertar",
                                        ]);
                                    },
                                            
                        /*'eliminar' => function ($url,$model,$key) {
                                return Html::a('<i class="fas fa-bomb"></i>', 
                                        [
                                        "site/eliminar",
                                        "id"=>$model->id                                       
                                        ]);
                                    },*/
                                            
                        // accion eliminar 
                        'eliminar' => function($url, $model){
                           return Html::a('<i class="fas fa-bomb"></i>', 
                                   ['site/eliminar', 'id' => $model->id], 
                                   [
                               'data' => [
                                       'confirm' => '¿Seguro que quieres eliminarlo?',
                                       'method' => 'post',
                               ],
                           ]);
                       },

                        'actualizar' => function ($url,$model,$key) {
                                return Html::a('<i class="fas fa-pen-nib"></i>', 
                                        [
                                        "site/actualizar",
                                        "id"=>$model->id
                                        ]);
                                    },
                        'ver' => function ($url,$model,$key) {
                                return Html::a('<i class="fas fa-eye"></i>', 
                                        [
                                        "site/ver",
                                        "id"=>$model->id    
                                        ]);
                                    },                    
                    ],
            ]   
    ] 
]);?>
</div>

<div class="row">
    <?= 
        Html::a('Crear ordenador',
            ['site/insertar'],
            ['class' => 'btn btn-primary']); 
    ?>  
</div>
