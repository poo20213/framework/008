<?php
use yii\bootstrap4\Accordion;
use yii\helpers\Html;
//METODO 1
    // colco los elementos a mano

    // preparar para la accion de red
    if ($model->ethernet) {
        $ethernet = '<i class="far fa-smile-beam"></i>';
    }else{
        $ethernet='<i class="far fa-dizzy"></i>';
    }

    if ($model->wifi) {
        $wifi = '<i class="fas fa-check"></i>';;
    }else{
        $wifi='<i class="fas fa-times"></i>';
    }
    // fin de la preparacion de red
    
    echo Accordion::widget([
     
    'items' => [
        [
            'label' => strtoupper($model->getAttributeLabel('descripcion')),
            'content' => $model->descripcion,
        ],
        
        [
            'label' => $model->getAttributeLabel('memoria'),
            'content' => $model->memoria,
        ],
        
        [
            'label' => $model->getAttributeLabel('video'),
            'content' => $model->video,
        ],
        
        [
            'label' => $model->getAttributeLabel('prcesador'),
            'content' => $model->procesador,
        ],
        
        [
            'label' => 'Red',
            'content' => $ethernet . 'Ethernet<br>' . $wifi . ' Wifi',
        ],
    ]
]);
    
    //FIN METODO 1

//----------------------------------------------------------------------------

/*
// METODO 2
// UTILIZANDO ARRAY
// creo un array con todos los elementos a mostrar
$elementos=[];

foreach ($model as $etiquetaCampo=>$valorCampo) {
    if ($etiquetaCampo=='id' || $etiquetaCampo=='disco_duro') {
        continue;
    }else{
        $elementos[]=[
            'label' => $etiquetaCampo,
            'content' => $valorCampo
        ];
    }
}


if ($model->ethernet) {
    $ethernet = '<i class="far fa-smile-beam"></i>';
}else{
    $ethernet='<i class="far fa-dizzy"></i>';
}

if ($model->wifi) {
    $wifi = '<i class="fas fa-check"></i>';;
}else{
    $wifi='<i class="fas fa-times"></i>';
}


$elementos[]=[
    'label' => 'Red',
    'content' => $ethernet . 'Ethernet<br>' . $wifi . ' Wifi'
];

echo Accordion::widget([
    'items' => $elementos,
    
]);
*/
 
    //FIN METODO 2

    
    echo Html::a("Ver normal",
        ['site/ver','id'=>$model->id],
        ['class'=>"btn btn-primary mt-2"]);