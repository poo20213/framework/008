<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Ordenadores */
/* @var $form ActiveForm */
?>
<div class="site-formulario">
    <h1><?=$titulo?></h1>
    <?php $form = ActiveForm::begin(); ?>
<?php
// creo array para guardar las opciones para los activeField
        $opciones=['template' => '{label}{input}',
            'inputOptions' => ['class' => 'col-lg-9'],
            'labelOptions' => ['class' => 'col-lg-2'],
            'options' => ['class' => 'row mb-2']    
            ];

        $opciones1=[
            'template' => '{label}{input}',
            'inputOptions' => ['class'=>'col-lg-9 p-0'],
            'labelOptions' => ['class'=>'col-lg-1 p-0'],
            'options' => ['form-check form-inline']
        ];

?>
       
        <?= $form->field($model, 'descripcion', $opciones)->textarea(["rows"=>6]) ?>
        <?= $form->field($model, 'procesador',$opciones) ?>
        <?= $form->field($model, 'memoria',$opciones) ?>
        <?= $form->field($model, 'disco_duro',$opciones) ?>
        <?= $form->field($model, 'video',$opciones) ?>
        <?php
            // con el checkbox como switch
            echo $form->field($model, 'ethernet',$opciones1)
            ->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'onText' => 'Con ethernet',
                    'offText' => 'Sin ethernet',
                ]
            ])->label(''); // quito el label del control
        ?>
    
        <?php
            // con el checkbox como switch
            echo $form->field($model, 'wifi')
            ->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'onText' => 'Con wifi',
                    'offText' => 'Sin wifi',
                ]
            ])->label(''); // quito el label del control
        ?>
        
        <?php 
            // con el checkbox normal
        
            /*echo $form->field($model, 'ethernet',$opciones1)
            ->checkbox(['class' => 'form-check-input'],false); */?>
    
        <?php
            /*echo $form->field($model, 'wifi',$opciones1)
            ->checkbox(['class' => 'form-check-input'],false);*/ ?>

        <div class="form-group">
            <?= Html::submitButton($accion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario -->
